let body = document.getElementsByTagName('body')
let loading = document.getElementById('loading')
var tabItem = document.getElementsByClassName("tab-item")
let tabContent = document.getElementsByClassName("tab-content")
let passwordInput = document.getElementById("password")
let passToggleIcon = document.getElementsByClassName("passToggle")[0]

// page loading
let loadingFunc = () => loading.remove()
body[0].onload = () => setTimeout(loadingFunc, 1500)

// default tab
tabContent[0].style.display = 'block'
tabItem[0].classList.add('card-tabs-active')

let activeLink = (tab) => {
    for (i = 0; i < tabItem.length; i++) {
        tabItem[i].classList.remove('card-tabs-active')
    }
    document.getElementById(tab).classList.add('card-tabs-active')
}

let activeContent = (content) => {
    setTimeout(() => {
    for (i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = 'none'
    }
    document.getElementById(content).style.display = 'block'
    }, 300)
}

let switchTab = (tab, content) => {
    activeLink(tab)
    activeContent(content)
}

// password toggle function
let passwordToggle = () => {
    if (passwordInput.type === "password")passwordInput.type = "text"
    else passwordInput.type = "password"

    passToggleIcon.classList.toggle('passToggle--toggled')
}